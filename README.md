## Web Scanning Demo

It is a sample project built using Node JS to demonstrate the using of [the open source web application twain](https://github.com/mgriit/ScanAppForWeb/).

## Installation

  - Clone the project.
  - In the terminal navigate to the project folder and run **npm start**
  - Open **http://localhost:3500** in the browser and test it.
