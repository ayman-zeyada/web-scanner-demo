const express = require('express');
const path = require('path');

const app = express();

app.set('port', process.env.PORT || 3500);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, 'public')));

app.get('*', function(req, res) {
    res.sendfile('./public/index.html');
});

app.listen(app.get('port'), () => {
    console.log('Server listining on port ' + app.get('port'));
});

module.exports = app;
